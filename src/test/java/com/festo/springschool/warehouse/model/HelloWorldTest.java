package com.festo.springschool.warehouse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HelloWorldTest {

    private HelloWorld underTest;

    @BeforeEach
    public void setUpEach() {
        underTest = new HelloWorld();
    }

    @Test
    public void testDoesGreetingReturnCorrectName() {
        // Classic assertions
        assertTrue(underTest.greet("Frodo").contains("Frodo"));
        // Fluent assertions
        assertThat(underTest.greet("Frodo")).contains("Frodo");
    }

}