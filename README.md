# Warehouse CLI Java-Template

This is the template project for Java development in the Festo Spring School. It should provide you a good starting point with some nice features built-in, which we'll explain in more detail as soon as the course starts. So far you'll get some hints here below for the most crucial commands and language specific topics.

## Setup

You can either use:
- The [GitPod](https://gitpod.io) integration which will spawn a VSCode in browser or you can link your local VSCode to the one Gitpod instance.
- Your own IDE and clone the repository to your local drive

We're making use of `Maven` as for package management. All dependencies and build information are stored in the pom.xml. We opted for a single application, but feel free to create a multi-module application if this helps you structuring your code.

Here are some basic maven commands:

- `mvn clean` clears the `target` folder in which the compiled application data is stored
- `mvn compile` compiles the application and downloads the necessary dependencies
- `mvn test` compiles the application and starts the unit tests.
- `mvn verify` compiles the application and executes the unit and integration tests.

## Getting Started

We provided you with some basic command line application as a starting point and some tests to show how it could be done. Feel free to implement your own structure. Nevertheless we suggest to keep the existing main folder structure:

- `src/main/java`: This is the folder where all source files belong to
- `src/main/resources`: This is the folder where non source files belong that are needed during compilation and runtime (e.g. the logging configuration)
- `src/test/java`: This is the folder where all test files belong to. You can name UnitTests like `*Test.java` and integration tests like `*IntTest.java`

## Libraries

- We added Spring Boot as IoC framework to wire everything together
- There is [JCommander](https://jcommander.org/) to comfortably parse the command line arguments

## Testing

- We integrated [JUnit 5](https://junit.org/) as testing framework and some examples in the tests folder
- We also added [AssertJ](https://assertj.github.io/) to have assertions in the tests that are better readable and more understandable